https://upc-academia-macfri.c9.io/teachers/


servicio web que ayudara a alimentar a los profesores en el caso de uso "Regitsrar Horas de clases para profesores"
este mismo desactivara a cada profesor cuando termine el caso de uso.


PARA GRABAR UN PROFESOR
curl -d "first_name=a" -d "last_name=martinez" -d "email=mac@gmail.com" -d "salary=122.00" -d "password=123" https://upc-academia-macfri.c9.io/teachers/teachers/

PARA LISTAR PROFESORES(LLENALO EN EL COMBO PROFESORES)
curl https://upc-academia-macfri.c9.io/teachers/teachers/

PARA DESACTIVAR AL PROFESOR(QUE YA NO SALGA EN EL COMBO DE PROFESORES)
curl -d "is_active=false" -XPATCH https://upc-academia-macfri.c9.io/teachers/teachers/7/

teachers/7/
7 => id del profesor



     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Welcome to your Django project on Cloud9 IDE!

Your Django project is already fully setup. Just click the "Run" button to start
the application. On first run you will be asked to create an admin user. You can
access your application from 'https://upc-academia-macfri.c9.io/' and the admin page from 
'https://upc-academia-macfri.c9.io/admin'.

## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

1) Run syncdb command to sync models to database and create Django's default superuser and auth system

    $ python manage.py syncdb

2) Run Django

    $ python manage.py runserver $IP:$PORT
    
## Support & Documentation

Django docs can be found at https://www.djangoproject.com/

You may also want to follow the Django tutorial to create your first application:
https://docs.djangoproject.com/en/1.7/intro/tutorial01/

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE.
To watch some training videos, visit http://www.youtube.com/user/c9ide