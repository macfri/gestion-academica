import soaplib
import datetime as dt

from soaplib.core.service import rpc, DefinitionBase
from soaplib.core.model.primitive import String, Integer
from soaplib.core.server import wsgi
from soaplib.core.model.clazz import Array
from soaplib.core.service import soap


class Service1(DefinitionBase):
    @soap(_returns=Array(String))
    def get_hours(self):
        return ['7:00 - 9:00', '9:00 - 11:00', '11:00 - 1:00']

if __name__=='__main__':
    try:
        from wsgiref.simple_server import make_server
        soap_application = soaplib.core.Application([Service1], 'tns')
        wsgi_application = wsgi.Application(soap_application)
        server = make_server('0.0.0.0', 8082, wsgi_application)
        print 'http://upc-academia-macfri.c9.io:8082/'
        server.serve_forever()
    except ImportError:
        print "Error: example server code requires Python >= 2.5"