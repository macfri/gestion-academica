# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=10)),
                ('name', models.CharField(max_length=100)),
                ('status', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('classroom', models.CharField(max_length=100)),
                ('datestart', models.DateField(max_length=100)),
                ('dateend', models.DateField(max_length=100)),
                ('mon', models.CharField(max_length=2, null=True)),
                ('tue', models.CharField(max_length=2, null=True)),
                ('wed', models.CharField(max_length=2, null=True)),
                ('thu', models.CharField(max_length=2, null=True)),
                ('fri', models.CharField(max_length=2, null=True)),
                ('course', models.ForeignKey(to='app.Course')),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=75)),
                ('is_active', models.BooleanField(default=False)),
                ('salary', models.FloatField()),
            ],
        ),
        migrations.AddField(
            model_name='schedule',
            name='teacher',
            field=models.ForeignKey(to='app.Teacher'),
        ),
    ]
