# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='code',
            field=models.CharField(unique=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='fri',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='mon',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='thu',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='tue',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='wed',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='email',
            field=models.EmailField(unique=True, max_length=254),
        ),
    ]
