from django.contrib import admin
from app.models import Teacher, Schedule, Course


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name','email', 'password', 'is_active', 'salary')

admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Schedule)
admin.site.register(Course)