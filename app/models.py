from django.db import models


class Teacher(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=75)
    is_active = models.BooleanField(default=False)
    salary = models.FloatField()

    def __unicode__(self):
        return self.email


class Course(models.Model):
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=100)
    status = models.BooleanField(default=False)


class Schedule(models.Model):
    course = models.ForeignKey(Course)
    teacher = models.ForeignKey(Teacher)
    classroom = models.CharField(max_length=100)
    datestart = models.DateField(max_length=100)
    dateend = models.DateField(max_length=100)
    mon = models.CharField(max_length=20, null=True)
    tue = models.CharField(max_length=20, null=True)
    wed = models.CharField(max_length=20, null=True)
    thu = models.CharField(max_length=20, null=True)
    fri = models.CharField(max_length=20, null=True)