from django.conf.urls import url, include
from django.views.generic import TemplateView

from rest_framework import routers, serializers, viewsets
from app.views import (show_courses, save_course, save_schedule, 
	save_teacher, TeacherViewSet, login, dashboard)

router = routers.DefaultRouter()
router.register(r'teachers', TeacherViewSet)

urlpatterns = [
    url(r'^rest/', include(router.urls)),
    url(r'^registrar-profesor.html/', save_teacher),
    url(r'^registrar-horario.html/', save_schedule),
    url(r'^registrar-curso.html/', save_course),
    url(r'^listar-cursos.html/', show_courses),
    url(r'^dashboard.html/', dashboard),
    url(r'^registrar-syllabus.html/', TemplateView.as_view(template_name="registrar-syllabus.html")),
    url(r'^registrar-asistencias.html/', TemplateView.as_view(template_name="registrar-asistencias.html")),
    url(r'^blog/1', TemplateView.as_view(template_name="blog/1.html")),
    url(r'^blog/2', TemplateView.as_view(template_name="blog/2.html")),
    url(r'^', login),
]