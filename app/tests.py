import json
import pika

from django.test import Client as TestClient
from django.conf import settings
from django.test import SimpleTestCase

from suds.client import Client as sudsClient


class UPCGestionUnitTest(SimpleTestCase):

	def test_connect_rest(self):
 		c = TestClient(enforce_csrf_checks=False)
 		params =  {
 			'first_name': 'xxx',
 			'last_name': 'smith',
 			'email': 'mac212@gmail.com',
 			'salary': 122.00,
 			'password': '123',
 		}
		response = c.post(settings.DOMAIN_URL+'rest/teachers/', params)
		self.assertEqual(response.status_code ,201)

	def test_connect_soap(self):
		proxy = sudsClient(settings.WSDL_URL)
		proxy.options.cache.clear()
		hours = proxy.service.get_hours().string
		self.assertEqual(['7:00 - 9:00', '9:00 - 11:00', '11:00 - 1:00'], hours)

	def test_queue(self):
		####### send message to queue
		message = 'TEST_OK'
		queue = 'test_queue'
		credentials = pika.PlainCredentials(settings.QUEUE_USERNAME, settings.QUEUE_PASSWORD)
		parameters = pika.ConnectionParameters(settings.QUEUE_HOSTNAME,
	                                           settings.QUEUE_PORT,
	                                           settings.QUEUE_USERNAME,
	                                           credentials)
		parameters.socket_timeout = 5
		connection = pika.BlockingConnection(parameters)
		channel = connection.channel()
		channel.queue_declare(queue=queue)
		channel.basic_publish(exchange='',
	                          routing_key=queue,
	                          body=message)
		####### receive message from queue
		method_frame, header_frame, body = channel.basic_get(queue = queue)        
		if method_frame:
			if method_frame.NAME != 'Basic.GetEmpty':
				channel.basic_ack(delivery_tag=method_frame.delivery_tag)
				_body = body
		connection.close()
		self.assertEqual(_body, message)