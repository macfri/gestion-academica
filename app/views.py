import json
import pika
import urllib2

from django.shortcuts import render, redirect
from django.views.generic import View
from django.conf import settings

from suds.client import Client

from app.models import Teacher, Schedule, Course
from rest_framework import serializers, viewsets


from django.test.runner import DiscoverRunner

#from django.test.simple import DjangoTestSuiteRunner

class NoDbTestRunner(DiscoverRunner):
    """A test suite runner that does not set up and tear down a database."""

    def setup_databases(self):
        """Overrides DjangoTestSuiteRunner"""
        pass

    def teardown_databases(self, *args):
        """Overrides DjangoTestSuiteRunner"""
        pass


class TeacherSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Teacher


class TeacherViewSet(viewsets.ModelViewSet):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


def save_course(request):
    if request.POST:
        course = Course()
        course.code = request.POST.get('code')
        course.name = request.POST.get('name')
        course.save()
    items = Course.objects.all()
    return render(request, 'registrar-curso.html', {
        'domain_url': settings.DOMAIN_URL, 'items': items})


def show_courses(request):
    return render(request, 'listar-cursos.html', {
        'items': Course.objects.all() , 'domain_url': settings.DOMAIN_URL})


def get_format_hours(hours):
    row_schedule_hours = {'mon': '', 'tue': '', 'wed': '', 'thu': '', 'fri': ''}
    for dayhour in hours:
        day, hour = dayhour.split('_')
        row_schedule_hours[day] = hour
    return row_schedule_hours


def save_schedule(request):
    code_course = request.GET.get('code')
    course = Course.objects.filter(code=code_course).first()
    if request.POST:
        teachaer_email = request.POST.get('teacher')
        teacher = Teacher.objects.filter(email=teachaer_email).first()
        hours = request.POST.getlist('hours')
        row_schedule_hours = get_format_hours(hours)
        shedule = Schedule()
        shedule.datestart = request.POST.get('datestart')
        shedule.dateend = request.POST.get('dateend')
        shedule.classroom = request.POST.get('classroom')
        shedule.course = course
        shedule.teacher = teacher
        shedule.mon = row_schedule_hours.get('mon')
        shedule.tue = row_schedule_hours.get('tue')
        shedule.wed = row_schedule_hours.get('wed')
        shedule.thu = row_schedule_hours.get('thu')
        shedule.fri = row_schedule_hours.get('fri')
        shedule.save()
        course.status = True
        course.save()
        send_message(teacher.email, course.name)
        return redirect(settings.DOMAIN_URL + 'listar-cursos.html/')

    proxy = Client(settings.WSDL_URL)
    proxy.options.cache.clear()
    hours = proxy.service.get_hours().string
    teachers = []    
    response = urllib2.urlopen('%srest/teachers/?format=json' % settings.DOMAIN_URL)
    html=response.read()
    x = json.loads(html)
    return render(request, 'registrar-horario.html', {
        'teachers': x, 'domain_url': settings.DOMAIN_URL, 'hours': hours, 'course': course})

    
def save_teacher(request):
    teachers = []
    response = urllib2.urlopen('%srest/teachers/?format=json' % settings.DOMAIN_URL)
    html=response.read()
    x = json.loads(html)
    return render(request, 'registrar-profesor.html', {
        'teachers': x, 'domain_url': settings.DOMAIN_URL})


def send_message(queue, course):
    credentials = pika.PlainCredentials(settings.QUEUE_USERNAME, settings.QUEUE_PASSWORD)
    parameters = pika.ConnectionParameters(settings.QUEUE_HOSTNAME,
                                           settings.QUEUE_PORT,
                                           settings.QUEUE_USERNAME,
                                           credentials)
    parameters.socket_timeout = 5
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='',
                          routing_key=queue,
                          body='Ha UD. se le asigno el curso %s' % course)
    connection.close()


def receive_message(queue):
    _body = ''
    credentials = pika.PlainCredentials(settings.QUEUE_USERNAME, settings.QUEUE_PASSWORD)
    parameters = pika.ConnectionParameters(settings.QUEUE_HOSTNAME,
                                           settings.QUEUE_PORT,
                                           settings.QUEUE_USERNAME,
                                           credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    method_frame, header_frame, body = channel.basic_get(queue = queue)        
    if method_frame:
        if method_frame.NAME == 'Basic.GetEmpty':
            connection.close()
        else:            
            channel.basic_ack(delivery_tag=method_frame.delivery_tag)
            _body = body
            connection.close()
    else:
        connection.close()
    return _body


def login(request):
    if request.POST:
        email = request.POST.get('email')
        password = request.POST.get('password')
        teacher = Teacher.objects.filter(email=email).first()
        if teacher:
            if teacher.password == password:
                return render(request, 'mostrar-mensaje.html', {
                    'body': receive_message(teacher.email), 
                    'domain_url': settings.DOMAIN_URL})
        return redirect(settings.DOMAIN_URL+'dashboard.html/')
    else:
        return render(request, 'login.html', {
            'domain_url': settings.DOMAIN_URL})


def dashboard(request):
    return render(request, 'base.html', {
        'domain_url': settings.DOMAIN_URL})
